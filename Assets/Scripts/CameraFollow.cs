﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    [SerializeField]
    private Transform target;
    [SerializeField]
    private float distance;
    [SerializeField]
    private ScrollingUVs_Layers scrollingUVs;
    [SerializeField]
    private float UVSpeedMultiplier = 1;

    private float previousZ;

    private void Start()
    {
        this.previousZ = this.transform.position.z;
    }

    void Update () {
        var dist = this.previousZ - this.transform.position.z;
        this.previousZ = this.transform.position.z;
        this.scrollingUVs.uvAnimationRate.y = -dist * this.UVSpeedMultiplier;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.target.position.z - distance);
        
	}

}
