﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameHandler : Singleton<GameHandler> {
    [SerializeField]
    private Player player;
    [SerializeField]
    private MeshRenderer tunnelRenderer;
    [SerializeField]
    private float difficultyProgression = 0.01f;

    public Player Player { get { return this.player; } }
    public int ScoreMultiplier { get; set; }
    public int Score { get; private set; }
    public int Distance { get; private set; }
    public float Difficulty { get; private set; }
    public bool IsGameOver { get; private set; }

    private int previousDistance = 0;
    private float startZ;
    private int perfectScores;

    private bool secondChanceUsed = false;

    public int HighScore
    {
        get
        {
            var highscore = PlayerPrefs.GetInt("Highscore", 0);
            if (this.Score > highscore)
            {
                PlayerPrefs.SetInt("Highscore", this.Score);
                highscore = this.Score;
            }
            return highscore;
        }
    }

    public int LongestDistance
    {
        get
        {
            var distance = PlayerPrefs.GetInt("LongestDistance", 0);
            if (this.Distance > distance)
            {
                PlayerPrefs.SetInt("LongestDistance", this.Distance);
                distance = this.Distance;
            }
            return distance;
        }
    }

    private  void Awake () {
        this.ResetParameters();
        this.UpdateSkins();

        var lastDay = PlayerPrefs.GetInt("LastDayPlayed", 0);
        if (lastDay != DateTime.Today.Day)
        {
            StatisticsRepository.Instance.PlayDays++;
            PlayerPrefs.SetInt("LastDayPlayed", DateTime.Today.Day);
        }
    }

    private void Update()
    {
        float dist = this.player.transform.position.z - this.startZ;
        this.Distance = (int)dist;
        if (StatisticsRepository.Instance.DistancePerGame < this.Distance)
        {
            StatisticsRepository.Instance.DistancePerGame = this.Distance;
        }
        if (this.previousDistance < this.Distance)
        {
            StatisticsRepository.Instance.TotalDistance += this.Distance - this.previousDistance;
        }
       
        UIManager.Instance.UpdateDistance();
    }

    public void IncreaseDifficulty()
    {
        this.Difficulty += (this.difficultyProgression * LevelProgressionManager.Instance.CurrentLevel);
        this.Difficulty = Mathf.Clamp(this.Difficulty, 0, 4);
    }

    public void StartGame()
    {
        StartCoroutine(PlatformManager.Instance.LoadPlatforms(this.EnableMovement));
        StatisticsRepository.Instance.PlayGames++;
    }

    public void EnableMovement()
    {
        this.player.AllowMovement = true;
    }

    public void AddScore()
    {
        this.Score += this.ScoreMultiplier;
        if (StatisticsRepository.Instance.PointsPerGame < this.Score)
        {
            StatisticsRepository.Instance.PointsPerGame = this.Score;
        }
        StatisticsRepository.Instance.TotalPoints += this.ScoreMultiplier;
        UIManager.Instance.UpdateScore();
        this.IncreaseDifficulty();
    }

    internal void IncreaseScoreMultiplier()
    {
        this.ScoreMultiplier++;
        this.perfectScores++;
        if (StatisticsRepository.Instance.PerfectScoresPerGame < this.perfectScores)
        {
            StatisticsRepository.Instance.PerfectScoresPerGame = this.perfectScores;
        }
    }

    public void ResetParameters()
    {
        this.IsGameOver = false;
        this.Score = 0;
        this.ScoreMultiplier = 1;
        this.Difficulty = 1;
        this.startZ = this.player.transform.position.z;
    }

    internal void ReloadLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    internal void Continue()
    {
        this.IsGameOver = false;
        this.player.Revive();
        PlatformManager.Instance.RotateToPlayer();
    }

    internal void GameOver()
    {
        if (SettingsManager.Instance.VibrateOn)
        {
            Handheld.Vibrate();
        }
        this.IsGameOver = true;
        this.player.AllowMovement = false;
        StatisticsRepository.Instance.DeathCount++;
        if (!this.secondChanceUsed)
        {
            this.secondChanceUsed = true;
            UIManager.Instance.Invoke("ShowContinue", 1);
        } else
        {
            UIManager.Instance.Invoke("ShowGameOver", 1);
        }

        AudioManager.Instance.PlayDeath();
    }

    internal void UpdateSkins()
    {
        this.player.UpdateSkin();
        this.tunnelRenderer.material.mainTexture = SkinRepository.Instance.CurrentVortexSkin.SkinTexture;
    }
}
