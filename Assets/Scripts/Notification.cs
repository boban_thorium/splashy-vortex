﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour {
    [SerializeField]
    private Image notificationBackground;
    [SerializeField]
    private Text notificationText;

    public int Amount {
        get
        {
            return PlayerPrefs.GetInt("NotificationAmount", 0);
        }

        set
        {
            PlayerPrefs.SetInt("NotificationAmount", value);
            this.UpdateNotifications();
        }
    }

    public void ClearNotifications()
    {
        this.Amount = 0;
    }

    public void UpdateNotifications()
    {
        this.notificationText.text = this.Amount.ToString();
        if (this.Amount == 0)
        {
            this.notificationBackground.enabled = false;
            this.notificationText.enabled = false;
        } else
        {
            this.notificationBackground.enabled = true;
            this.notificationText.enabled = true;
        }
    }
}

