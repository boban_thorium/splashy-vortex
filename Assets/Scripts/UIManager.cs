﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text distanceText;
    [Header("Game Over Window")]
    [SerializeField]
    private GameObject gameOverPanel;
    [SerializeField]
    private Text gameOverScore;
    [SerializeField]
    private Text gameOverHighScore;
    [SerializeField]
    private Text gameOverLongestDistance;
    [SerializeField]
    private Text gameOverDistance;
    [Space]
    [SerializeField]
    private GameObject startPanel;
    [SerializeField]
    private ContinuePanel continuePanel;
    [SerializeField]
    private SettingsPanel settingsPanel;
    [SerializeField]
    private SkinsPanel skinsPanel;
    [SerializeField]
    private GameObject diamondPanel;
    [SerializeField]
    private Text diamondText;
    [SerializeField]
    private Notification notification;
    [SerializeField]
    private GameObject adsErrorPanel;
    [SerializeField]
    private LevelProgressionPanel[] levelProgressionPanels;
    [Space]
    [SerializeField]
    private UnityEvent onSkinUnlocked;


    private void Awake()
    {
        this.HideGameOver();
        this.ShowStartPanel();
        this.HideContinue();
        this.ShowDiamonds();
        this.HideSkins();
        this.distanceText.gameObject.SetActive(false);
        this.scoreText.gameObject.SetActive(false);
        this.HideSettingsPanel();
        this.UpdateScore();
        this.UpdateDistance();
        this.notification.UpdateNotifications();
        this.HideAdsErrorPanel();
        this.UpdateLevelProgressionPanels();
    }

    internal void SkinUnlocked()
    {
        this.onSkinUnlocked.Invoke();
        this.notification.Amount++;
    }

    public void UpdateScore()
    {
        this.scoreText.text = GameHandler.Instance.Score.ToString();
    }

    public void UpdateDistance()
    {
        this.distanceText.text = GameHandler.Instance.Distance.ToString() + "m";
    }

    public void StartGame()
    {
        GameHandler.Instance.StartGame();
        this.HideStartPanel();
        this.distanceText.gameObject.SetActive(true);
        this.scoreText.gameObject.SetActive(true);
    }

    public void ShowStartPanel()
    {
        this.startPanel.SetActive(true);
    }

    public void HideStartPanel()
    {
        this.startPanel.SetActive(false);
    }

    public void ShowSettingsPanel()
    {
        this.settingsPanel.gameObject.SetActive(true);
    }

    public void HideSettingsPanel()
    {
        this.settingsPanel.gameObject.SetActive(false);
    }

    public void HideGameOver()
    {
        this.gameOverPanel.SetActive(false);
    }

    public void UpdateDiamonds()
    {
        this.diamondText.text = Bank.Instance.Diamonds.ToString();
    }

    public void HideDiamonds()
    {
        this.diamondPanel.SetActive(false);
    }

    public void ShowDiamonds()
    {
        this.diamondPanel.SetActive(true);
        this.UpdateDiamonds();
    }

    public void HideAdsErrorPanel()
    {
        this.adsErrorPanel.SetActive(false);
    }

    public void ShowAdsErrorPanel()
    {
        this.adsErrorPanel.SetActive(true);
    }

    public void ShowContinue()
    {
        this.continuePanel.gameObject.SetActive(true);
        this.StartCoroutine(this.continuePanel.Initialise(this.Continue, this.ShowGameOver, this.ShowAdsErrorPanel));
    }

    public void ShowSkins()
    {
        this.skinsPanel.gameObject.SetActive(true);
    }

    public void HideSkins()
    {
        this.skinsPanel.gameObject.SetActive(false);
        this.UpdateSkins();
    }

    public void HideContinue()
    {
        this.continuePanel.gameObject.SetActive(false);
    }

    public void UpdateSkins()
    {
        GameHandler.Instance.UpdateSkins();
    }

    public void ShowGameOver()
    {
        this.HideContinue();
        this.gameOverPanel.SetActive(true);
        this.gameOverScore.text = GameHandler.Instance.Score.ToString();
        this.gameOverHighScore.text = GameHandler.Instance.HighScore.ToString();
        this.gameOverDistance.text = GameHandler.Instance.Distance.ToString() + "m";
        this.gameOverLongestDistance.text = GameHandler.Instance.LongestDistance.ToString() + "m";
    }

    private void Continue()
    {
        this.HideContinue();
        GameHandler.Instance.Continue();
    }

    public void Restart()
    {
        GameHandler.Instance.ReloadLevel();
    }

    public void UpdateLevelProgressionPanels()
    {
        foreach(LevelProgressionPanel lPP in this.levelProgressionPanels)
        {
            lPP.UpdatePanel();
        }
    }
}
