﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgressionManager : Singleton<LevelProgressionManager>
{
    [SerializeField]
    private int levelGoal;

    public int CurrentLevel
    {
        get { return PlayerPrefs.GetInt("CurrentLevel", 1); }
        set { PlayerPrefs.SetInt("CurrentLevel", value);}
    }

    public void CheckLevelUp()
    {
        if (StatisticsRepository.Instance.TotalPoints > this.CurrentGoal)
        {
            this.CurrentLevel++;
            UIManager.Instance.UpdateLevelProgressionPanels();
        }
    }

    public int CurrentGoal
    {
        get
        {
            int amount = 0;
            for (int i = 0; i < this.CurrentLevel; i++)
            {
                amount += i * this.levelGoal;
            }
            amount += this.levelGoal * this.CurrentLevel;
            return amount;
        }
    }

    public int PreviousGoal
    {
        get
        {
            int amount = 0;
            for (int i = 0; i < this.CurrentLevel; i++)
            {
                amount += i * this.levelGoal;
            }
            return amount;
        }
    }

    public float GoalNormalized
    {
        get
        {
            int currentScore = StatisticsRepository.Instance.TotalPoints - this.PreviousGoal;
            print(currentScore / (this.CurrentLevel * this.levelGoal));
            return (float)currentScore / ((float)this.CurrentLevel * (float)this.levelGoal);
        }
    }
}
