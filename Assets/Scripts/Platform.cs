﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
    [SerializeField]
    private bool swing = false;
    [SerializeField]
    private float angleSwing;
    [SerializeField]
    private AnimationCurve swingCurve;
    [SerializeField]
    [Range(0,1)]
    private float swingSpeed = 0.01f;
    [Header("Gradient Animations")]
    [SerializeField]
    private GradientAnimation perfectZoneGradientAnimation;
    [SerializeField]
    private GradientAnimation platformGradientAnimation;
    [SerializeField]
    private ScoreIndicator scoreIndicatorPrefab;

    private ScoreIndicator scoreIndicator;
    public bool EnableSwing { get; set; }
    private bool tagged = false;
    private Animator animator;

    private void Start()
    {
        this.scoreIndicator = Instantiate(this.scoreIndicatorPrefab, this.transform.position, this.transform.rotation, this.transform);
        this.EnableSwing = this.swing;
        this.animator = this.GetComponent<Animator>();
        if (this.EnableSwing)
        {
            StartCoroutine(this.Swing());
        }    
    }

    public void Tag(bool perfect)
    {
        if (this.tagged)
        {
            return;
        }
        this.animator.SetTrigger("Tagged");
        if (perfect)
        {
            GameHandler.Instance.IncreaseScoreMultiplier();
            StartCoroutine(this.perfectZoneGradientAnimation.Play());
            AudioManager.Instance.PlayPlatformPerfect();
            StatisticsRepository.Instance.TotalPerfectScores++;
        }
        else
        {
            AudioManager.Instance.PlayPlatform();
            StartCoroutine(this.platformGradientAnimation.Play());
        }
        this.scoreIndicator.ShowScore(GameHandler.Instance.ScoreMultiplier, perfect);
        GameHandler.Instance.AddScore();
        PlatformManager.Instance.CreatePlatform();
        PlatformManager.Instance.RemovePlatform(this);
        
    }


    public IEnumerator Swing()
    {
        Vector3 startEuler = this.transform.localEulerAngles;
        while (this.EnableSwing)
        {
            float t = 0;
            while (t < this.swingCurve.keys[this.swingCurve.length-1].time)
            {
                t += this.swingSpeed * Time.timeScale;
                float eval = this.swingCurve.Evaluate(t);
                var zRot = startEuler.z + (eval * this.angleSwing);
                this.transform.localEulerAngles = new Vector3(startEuler.x, startEuler.y, zRot);
                if (!this.EnableSwing)
                {
                    break;
                }
                yield return null;
            }
        }
    }

    [ContextMenu("SetChild")]
    public void SetPerfectChild()
    {
        Transform t = this.transform.Find("Perfect");
        if (t != null)
        {
            t.SetParent(this.transform.Find("Platform"));
        }
        
    }


[System.Serializable]
    public class GradientAnimation
    {
        [SerializeField]
        private Renderer renderer;
        [SerializeField]
        private Gradient gradient;
        [SerializeField]
        private float duration;

        public IEnumerator Play()
        {
            float t = 0;
            Color originalColor = this.renderer.sharedMaterial.color;
            while (t < this.duration)
            {
                t += Time.deltaTime;
                t = Mathf.Clamp(t, 0, this.duration);
                float eval = t / this.duration;
                this.renderer.material.SetColor("_Color", this.gradient.Evaluate(eval));
                yield return new WaitForEndOfFrame();
            }
            this.renderer.material.SetColor("_Color", originalColor);
        }

    }
}
