﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Skin", menuName = "Splashy Vortex/Skin", order = 1)]
public class Skin : ScriptableObject
{
    [SerializeField]
    private SkinPrefab skinPrefab;
    [SerializeField]
    private Texture2D skinTexture;
    [SerializeField]
    private Sprite previewImage;
    [SerializeField]
    private Achievement achievement;
    [SerializeField]
    private bool paid = false;
    [SerializeField]
    private int price;


    public bool Paid
    {
        get
        {
            return this.paid;
        }
    }

    public int Price
    {
        get
        {
            return this.price;
        }
    }

    public bool Unlocked {
        get
        {
            
            if (this.paid)
            {
                var unlocked = PlayerPrefs.GetInt(this.name, 0);
                if (unlocked == 0)
                {
                    return false;
                } else
                {
                    return true;
                }
            } else {
                if (this.achievement == null)
                {
                    return true;
                }
                return this.achievement.Unlocked;
            }
        }
    }

    public void BuySkin()
    {
        PlayerPrefs.SetInt(this.name, 1);
        Bank.Instance.Diamonds -= this.price;
    }

    public Achievement Achievement { get { return this.achievement; } }
    public SkinPrefab SkinPrefab { get { return this.skinPrefab; } }
    public Sprite PreviewImage { get { return this.previewImage; } }
    public Texture2D SkinTexture { get { return this.skinTexture; } }

}


