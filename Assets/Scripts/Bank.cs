﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Bank : Singleton<Bank>
{

    public int Diamonds {
        get {
            return PlayerPrefs.GetInt("Diamonds", 0);
        }

        set {
            PlayerPrefs.SetInt("Diamonds", value);
            UIManager.Instance.UpdateDiamonds();
        }
    }
}
