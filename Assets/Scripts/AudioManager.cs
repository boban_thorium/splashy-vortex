﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager> {
    [SerializeField]
    private AudioSource platformSFX;
    [SerializeField]
    private AudioSource platformPerfectSFX;
    [SerializeField]
    private AudioSource diamondPickSFX;
    [SerializeField]
    private AudioSource deathSFX;
    
    public void PlayPlatform()
    {
        if (SettingsManager.Instance.AudioOn)
        {
            this.platformSFX.Play();
        }
    }

    public void PlayPlatformPerfect()
    {
        if (SettingsManager.Instance.AudioOn)
        {
            this.platformPerfectSFX.Play();
        }
    }

    public void PlayDiamondPick()
    {
        if (SettingsManager.Instance.AudioOn)
        {
            this.diamondPickSFX.Play();
        }
    }

    public void PlayDeath()
    {
        if (SettingsManager.Instance.AudioOn)
        {
            this.deathSFX.Play();
        }
    }

}
