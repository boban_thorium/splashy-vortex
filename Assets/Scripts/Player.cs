﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField]
    private AnimationCurve jumpingCurve;
    [SerializeField]
    private float jumpHeight;

    private SkinPrefab currentSkinPrefab;
    public bool AllowMovement { get; set; }
    public float MovementSpeed { get; set; }
    private Vector3 currentPosition;
    private Vector3 targetPosition;
    private int diamondsPicked;
   

    private void Start()
    {
        this.AllowMovement = false;
        this.MovementSpeed = 1;
        this.StartCoroutine(this.Jump());
    }

    private IEnumerator Jump()
    {
        while(!GameHandler.Instance.IsGameOver)
        {
            this.currentPosition = this.transform.position;
            this.targetPosition = new Vector3(this.transform.position.x, this.jumpHeight, this.transform.position.z + PlatformManager.Instance.PlatformDistance);
            float t = 0;
            bool move = this.AllowMovement;
            while (t < 1)
            {
                
                t += (0.02f * this.MovementSpeed * GameHandler.Instance.Difficulty) * Time.timeScale;
                t = Mathf.Clamp(t, 0, 1);
                float eval = this.jumpingCurve.Evaluate(t);
                float y = Mathf.Lerp(this.currentPosition.y, this.targetPosition.y, eval);
                float z = Mathf.Lerp(this.currentPosition.z, this.targetPosition.z, t);
                if (!move)
                {
                    z = this.currentPosition.z;
                }
                this.transform.position = new Vector3(this.transform.position.x, y, z);
                yield return new WaitForEndOfFrame();
            }
            if (move)
            {
                RaycastHit hit;
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1.5f, Color.yellow);
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 1.5f))
                {
                    if (hit.transform.tag == "Platform")
                    {
                        hit.transform.SendMessageUpwards("Tag", false);
                    }

                    if (hit.transform.tag == "PlatformPerfect")
                    {
                        hit.transform.parent.SendMessageUpwards("Tag", true);
                    }

                    if (hit.transform.tag == "Tunnel" || hit.transform.tag == "Spikes")
                    {
                        this.currentSkinPrefab.Explode();
                        GameHandler.Instance.GameOver();
                    }

                    
                }

            }
        }
        
    }

    public void UpdateSkin()
    {
        if (this.currentSkinPrefab != null)
        {
            Destroy(this.currentSkinPrefab.gameObject);
        }
        this.currentSkinPrefab = Instantiate(SkinRepository.Instance.CurrentBallSkin.SkinPrefab, this.transform.position, Quaternion.identity, this.transform);
    }

    internal void Revive()
    {
        this.currentSkinPrefab.Show();
        this.StartCoroutine(this.Jump());
        this.Invoke("EnableMovement", 1);
    }

    private void EnableMovement()
    {
        this.AllowMovement = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Diamond")
        {
            Bank.Instance.Diamonds++;
            AudioManager.Instance.PlayDiamondPick();
            this.diamondsPicked++;
            if (StatisticsRepository.Instance.DiamondsPerGame < this.diamondsPicked)
            {
                StatisticsRepository.Instance.DiamondsPerGame = this.diamondsPicked;
            }
            StatisticsRepository.Instance.TotalDiamonds++;
            Destroy(other.transform.gameObject);
        }

    }
}
