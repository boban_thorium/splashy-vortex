﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Achievement", menuName = "Splashy Vortex/Achievement", order = 1)]
public class Achievement : ScriptableObject
{
    [SerializeField]
    private string description;
    [SerializeField]
    private ParameterType parameterType;
    [SerializeField]
    private int goal;

    public int Goal { get { return this.goal;} }
    public ParameterType ParamType { get { return this.parameterType; } }
    public string Description { get { return this.description; } }

    public bool Unlocked
    {
        get
        {
            if (this.CurrentStatus >= this.goal)
            {
                var unlocked = PlayerPrefs.GetInt(this.name, 0);
                if (unlocked == 0)
                {
                    PlayerPrefs.SetInt(this.name, 1);
                    UIManager.Instance.SkinUnlocked();
                }

                return true; 
            } else
            {
                return false;
            }
        }
    }

    public int CurrentStatus {
        get
        {
            if (this.parameterType == ParameterType.DistancePerGame)
            {
                return StatisticsRepository.Instance.DistancePerGame;
            }

            if (this.parameterType == ParameterType.PerfectScoresPerGame)
            {
                return StatisticsRepository.Instance.PerfectScoresPerGame;
            }

            if (this.parameterType == ParameterType.PlayDays)
            {
                return StatisticsRepository.Instance.PlayDays;
            }

            if (this.parameterType == ParameterType.PlayGames)
            {
                return StatisticsRepository.Instance.PlayGames;
            }

            if (this.parameterType == ParameterType.PointsPerGame)
            {
                return StatisticsRepository.Instance.PointsPerGame;
            }

            if (this.parameterType == ParameterType.TotalDistance)
            {
                return StatisticsRepository.Instance.TotalDistance;
            }

            if (this.parameterType == ParameterType.TotalPerfectScores)
            {
                return StatisticsRepository.Instance.TotalPerfectScores;
            }

            if (this.parameterType == ParameterType.TotalPoints)
            {
                return StatisticsRepository.Instance.TotalPoints;
            }

            if (this.parameterType == ParameterType.DeathCount)
            {
                return StatisticsRepository.Instance.DeathCount;
            }

            if (this.parameterType == ParameterType.DiamondsPerGame)
            {
                return StatisticsRepository.Instance.DiamondsPerGame;
            }

            if (this.parameterType == ParameterType.TotalDiamonds)
            {
                return StatisticsRepository.Instance.TotalDiamonds;
            }

            return 0;
        }
    }

    public float GetGoalPercentage()
    {
        return (float)this.CurrentStatus / (float)this.goal;
    }

    public enum ParameterType {DistancePerGame, TotalDistance, PointsPerGame, TotalPoints,PlayGames,PlayDays, PerfectScoresPerGame, TotalPerfectScores, DeathCount, DiamondsPerGame, TotalDiamonds }
}
