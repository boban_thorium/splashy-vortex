﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatisticsRepository : Singleton<StatisticsRepository> {
   

    public int DistancePerGame { 
        get { return PlayerPrefs.GetInt("DistancePerGame", 0); }
        set { PlayerPrefs.SetInt("DistancePerGame", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int TotalDistance
    {
        get { return PlayerPrefs.GetInt("TotalDistance", 0); }
        set { PlayerPrefs.SetInt("TotalDistance", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int TotalPoints
    {
        get { return PlayerPrefs.GetInt("TotalPoints", 0); }

        set {
            PlayerPrefs.SetInt("TotalPoints", value);
            SkinRepository.Instance.CheckAchievementUnlocked();
            LevelProgressionManager.Instance.CheckLevelUp();
            UIManager.Instance.UpdateLevelProgressionPanels();
        }
    }

    public int PointsPerGame {
        get { return PlayerPrefs.GetInt("PointsPerGame", 0); }
        set { PlayerPrefs.SetInt("PointsPerGame", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int PlayGames {
        get { return PlayerPrefs.GetInt("PlayGames", 0); }
        set { PlayerPrefs.SetInt("PlayGames", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int PlayDays {
        get { return PlayerPrefs.GetInt("PlayDays", 0); }
        set { PlayerPrefs.SetInt("PlayDays", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int PerfectScoresPerGame
    {
        get { return PlayerPrefs.GetInt("PerfectScoresPerGame", 0); }
        set { PlayerPrefs.SetInt("PerfectScoresPerGame", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int TotalPerfectScores
    {
        get { return PlayerPrefs.GetInt("TotalPerfectScores", 0); }
        set { PlayerPrefs.SetInt("TotalPerfectScores", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int TotalDiamonds
    {
        get { return PlayerPrefs.GetInt("TotalDiamonds", 0); }
        set { PlayerPrefs.SetInt("TotalDiamonds", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int DiamondsPerGame
    {
        get { return PlayerPrefs.GetInt("DiamondsPerGame", 0); }
        set { PlayerPrefs.SetInt("DiamondsPerGame", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }

    public int DeathCount
    {
        get { return PlayerPrefs.GetInt("DeathCount", 0); }
        set { PlayerPrefs.SetInt("DeathCount", value); SkinRepository.Instance.CheckAchievementUnlocked(); }
    }


}
