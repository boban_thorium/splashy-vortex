﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : Singleton<AdsManager> {
    private OnSuccess onSuccess;
    private OnFailed onFailed;
    private OnSkipped onSkipped;
    public delegate void OnSuccess();
    public delegate void OnSkipped();
    public delegate void OnFailed();

    public void ShowRewardedAd(OnSuccess _onSuccess, OnSkipped _onSkipped, OnFailed _onFailed)
    {
        this.onSuccess = _onSuccess;
        this.onFailed = _onFailed;
        this.onSkipped = _onSkipped;
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        } else
        {
            this.onFailed.Invoke();
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                this.onSuccess.Invoke();
                break;
            case ShowResult.Skipped:
                this.onSkipped.Invoke();
                break;
            case ShowResult.Failed:
                this.onFailed.Invoke();
                break;
        }
    }
}
