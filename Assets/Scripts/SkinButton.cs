﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinButton : MonoBehaviour {
    [SerializeField]
    private Image skinIcon;
    [SerializeField]
    private Image unlockBar;
    [SerializeField]
    private GameObject priceTag;
    [SerializeField]
    private Text priceText;

    private Toggle toggle;
    public Toggle Toggle {
        get
        {
            if (this.toggle == null)
            {
                this.toggle = this.GetComponent<Toggle>();
            }
            return this.toggle;
        } 
    }
    public int SkinIndex { get; set; }
    public Skin AssignedSkin { get; internal set; }
    private SkinsPanel skinsPanel;


    internal void Setup(SkinsPanel _skinsPanel, Skin skin, int index)
    {
        this.skinsPanel = _skinsPanel;
        this.AssignedSkin = skin;
        this.SkinIndex = index;
        this.skinIcon.sprite = this.AssignedSkin.PreviewImage;
        this.priceTag.SetActive(false);
        if (skin.Unlocked)
        {
            this.unlockBar.fillAmount = 0;
        } else
        {
            if (skin.Paid)
            {
                this.unlockBar.fillAmount = 1;
                this.priceTag.SetActive(true);
                this.priceText.text = skin.Price.ToString();
            }
            else
            {
                this.unlockBar.fillAmount = Mathf.Abs(this.AssignedSkin.Achievement.GetGoalPercentage() - 1);
            }
        }
        
        
    }
}
