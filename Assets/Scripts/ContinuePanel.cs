﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinuePanel : MonoBehaviour {
    [SerializeField]
    private Image loadingBar;
    [SerializeField]
    private Text countdownText;
    [SerializeField]
    private int duration = 5;

    private bool continued = false;
    private OnContinue onContinue;
    private OnCancel onCancel;
    private OnAdError onAdError;
    public delegate void OnContinue();
    public delegate void OnCancel();
    public delegate void OnAdError();

    public IEnumerator Initialise(OnContinue onContinue, OnCancel onCancel, OnAdError onAdError)
    {
        float t = this.duration;
        this.continued = false;
        this.onContinue = onContinue;
        this.onCancel = onCancel;
        this.onAdError = onAdError;
        while (t > 0)
        {
            t -= Time.deltaTime;
            t = Mathf.Clamp(t, 0, 5);
            float normalized = t / this.duration;
            this.loadingBar.fillAmount = normalized;
            int seconds = Mathf.CeilToInt(t);
            this.countdownText.text = seconds.ToString();
            if (this.continued)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }

        if (!this.continued)
        {
            this.onCancel.Invoke();
        }
    }

    private void OnContinueSuccess()
    {
        this.onContinue.Invoke();
    }

    private void OnContinueFailed()
    {
        this.onAdError.Invoke();
        this.onCancel.Invoke();
    }

    private void OnContinueCancel()
    {
        this.onCancel.Invoke();
    }

    public void Continue()
    {
        this.continued = true;
        AdsManager.Instance.ShowRewardedAd(this.OnContinueSuccess, this.OnContinueCancel, this.OnContinueFailed);
    }

}
