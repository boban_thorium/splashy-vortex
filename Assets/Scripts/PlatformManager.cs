﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : Singleton<PlatformManager>
{
    [SerializeField]
    private Platform firstPlatform;
    [SerializeField]
    private PlatformPreset[] platformPresets;
    [SerializeField]
    private float platformDistance;
    [SerializeField]
    private int preloadAmount = 20;
    [SerializeField]
    private float startAngle = 10;
    [SerializeField]
    [Range(0, 2)]
    private float rotateSensitivity;

    private bool pressed = false;
    private OnPlatformsLoaded onPlatformsLoaded;
    public delegate void OnPlatformsLoaded();

    private float AngleRange {
        get {
            var angle = this.startAngle * GameHandler.Instance.Difficulty;

            return Mathf.Clamp(angle,0, 120);
        }
    }
    public float PlatformDistance { get { return this.platformDistance; } }

    private float previousMouseX;

    private List<Platform> instantiatedPlatforms = new List<Platform>();

    private void Start()
    {
        this.instantiatedPlatforms.Add(this.firstPlatform);
        this.firstPlatform.transform.parent = this.transform;
        
    }

    public IEnumerator LoadPlatforms(OnPlatformsLoaded callback)
    {
        for (int i = 0; i < this.preloadAmount; i++)
        {
            this.CreatePlatform();
            yield return new WaitForSeconds(Time.deltaTime);
        }
        callback.Invoke();
    }

    internal void RemovePlatform(Platform platform)
    {
        Platform platformToRemove = platform;
        this.instantiatedPlatforms.Remove(platform);
        Destroy(platformToRemove.gameObject, 1);
    }

    private void Update()
    {
        if (GameHandler.Instance.IsGameOver || !GameHandler.Instance.Player.AllowMovement)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            this.previousMouseX = Input.mousePosition.x;
            this.pressed = true;
        }

        if (Input.GetMouseButton(0) && this.pressed)
        {
            var dist = Input.mousePosition.x - this.previousMouseX;
            this.transform.Rotate(0, 0, dist * this.rotateSensitivity);
            this.previousMouseX = Input.mousePosition.x;
        }

        if (Input.GetMouseButtonUp(0))
        {
            this.pressed = false;
        }
    }

    public void RotateToPlayer()
    {
        this.instantiatedPlatforms[1].EnableSwing = false;
        float angle = this.instantiatedPlatforms[1].transform.localEulerAngles.z;
        this.transform.eulerAngles = new Vector3(0, 0, -angle);
    }

    private Platform GetRandomPlatform()
    {
        List<Platform> availablePlatform = new List<Platform>();
        for(int i = 0; i < this.platformPresets.Length; i++) {
            if (this.platformPresets[i].LevelStart <= LevelProgressionManager.Instance.CurrentLevel)
            {
                availablePlatform.Add(this.platformPresets[i].PlatformPrefab);
            }
        }
        return availablePlatform[UnityEngine.Random.Range(0, availablePlatform.Count - 1)];
    }

    public void CreatePlatform()
    {

        Platform p = Instantiate(this.GetRandomPlatform(), Vector3.zero, Quaternion.identity, this.transform);
        Vector3 pos = this.instantiatedPlatforms[this.instantiatedPlatforms.Count - 1].transform.position;
        Vector3 angle = this.instantiatedPlatforms[this.instantiatedPlatforms.Count - 1].transform.localEulerAngles;

        pos.z += this.platformDistance;
        p.transform.position = pos;
        p.transform.localEulerAngles = angle;
        p.transform.Rotate(0, 0, UnityEngine.Random.Range(-this.AngleRange, this.AngleRange));
        this.instantiatedPlatforms.Add(p);
    }

    [System.Serializable]
    public struct PlatformPreset
    {
        [SerializeField]
        private Platform platformPrefab;
        [SerializeField]
        private int levelStart;

        public float LevelStart { get { return this.levelStart; } }
        public Platform PlatformPrefab { get { return this.platformPrefab; } }
    }
}

