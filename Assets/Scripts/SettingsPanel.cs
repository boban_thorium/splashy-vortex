﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : MonoBehaviour {
    [SerializeField]
    private Toggle soundToggle;
    [SerializeField]
    private Toggle vibrateToggle;

    private void OnEnable()
    {
        this.soundToggle.isOn = SettingsManager.Instance.AudioOn;
        this.vibrateToggle.isOn = SettingsManager.Instance.VibrateOn;
    }

    public void ToggleSound()
    {
        SettingsManager.Instance.AudioOn = this.soundToggle.isOn;
    }

    public void ToggleVibrate()
    {
        SettingsManager.Instance.VibrateOn = this.vibrateToggle.isOn;
    }
}
