﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinRepository : Singleton<SkinRepository> {
    [SerializeField]
    private Skin[] ballSkins;
    [SerializeField]
    private Skin[] vortexSkins;
    public Skin[] BallSkins { get { return this.ballSkins; } }
    public Skin[] VortexSkins { get { return this.vortexSkins; } }

    public int CurrentBallSkinIndex {
        get {
            return PlayerPrefs.GetInt("CurrentBallSkinIndex", 0);
        }
        set {
            PlayerPrefs.SetInt("CurrentBallSkinIndex", value);
        }
    }

    public int CurrentVortexSkinIndex
    {
        get
        {
            return PlayerPrefs.GetInt("CurrentVortexSkinIndex", 0);
        }
        set
        {
            PlayerPrefs.SetInt("CurrentVortexSkinIndex", value);
        }
    }

    public Skin CurrentBallSkin
    {
        get
        {
            return this.ballSkins[this.CurrentBallSkinIndex];
        }
    }

    public Skin CurrentVortexSkin
    {
        get
        {
            return this.vortexSkins[this.CurrentVortexSkinIndex];
        }
    }

    public int TotalSkins
    {
        get
        {
            return this.ballSkins.Length + this.vortexSkins.Length;
        }
    }

    public int TotalUnlockedSkins
    {
        get
        {
            int unlocked = 0;
            foreach (Skin s in this.ballSkins)
            {
                if (s.Unlocked)
                {
                    unlocked++;
                }
            }

            foreach (Skin s in this.vortexSkins)
            {
                if (s.Unlocked)
                {
                    unlocked++;
                }
            }

            return unlocked;
        }
    }

    public void CheckAchievementUnlocked()
    {
        foreach (Skin sk in this.ballSkins)
        {
            if (sk.Achievement != null)
            {
                bool check = sk.Achievement.Unlocked;
            }
            
        }
        foreach (Skin sk in this.vortexSkins)
        {
            if (sk.Achievement != null)
            {
                bool check = sk.Achievement.Unlocked;
            }
        }
    }

}
