﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressionPanel : MonoBehaviour
{
    [SerializeField]
    private Image progressionFill;
    [SerializeField]
    private Text levelText;

    public void UpdatePanel()
    {
        this.progressionFill.fillAmount = LevelProgressionManager.Instance.GoalNormalized;
        this.levelText.text = LevelProgressionManager.Instance.CurrentLevel.ToString();
    }
}
