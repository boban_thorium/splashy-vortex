﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkinsPanel : MonoBehaviour {
    [SerializeField]
    private Image skinsUnlockedBar;
    [SerializeField]
    private Text skinsUnlockedStatus;
    [SerializeField]
    private GameObject skinPreviewPanel;
    [SerializeField]
    private Text achievementText;
    [SerializeField]
    private Text achievementStatusText;
    [SerializeField]
    private Image skinPreviewImage;
    [SerializeField]
    private Button skinBuyButton;
    [SerializeField]
    private Text skinPriceText;
    [SerializeField]
    private SkinButton[] skinButtons;
    [SerializeField]
    private Toggle ballToggle;
    [SerializeField]
    private Toggle vortexToggle;

    private SkinsShown skinsShown;
    private enum SkinsShown { Vortex, Ball };
    private Skin selectedSkin;

    private void OnEnable()
    {
        this.ballToggle.isOn = true;
        this.vortexToggle.isOn = false;
        this.UpdateSkinsUnlockedBar();
        this.HideSkinPreview();
        this.ShowBallSkins();
    }

    public void UpdateSkinsUnlockedBar()
    {
        this.skinsUnlockedBar.fillAmount = (float)SkinRepository.Instance.TotalUnlockedSkins / (float)SkinRepository.Instance.TotalSkins;
        this.skinsUnlockedStatus.text = SkinRepository.Instance.TotalUnlockedSkins + "/" + SkinRepository.Instance.TotalSkins;
    }

    public void ShowVortexSkins()
    {
        this.HideAllSkinButtons();
        this.skinsShown = SkinsShown.Vortex;
        for (int i = 0; i < SkinRepository.Instance.VortexSkins.Length; i++)
        {
            this.skinButtons[i].gameObject.SetActive(true);
            this.skinButtons[i].Setup(this, SkinRepository.Instance.VortexSkins[i], i);
            this.skinButtons[i].Toggle.isOn = false;
        }
        this.skinButtons[SkinRepository.Instance.CurrentVortexSkinIndex].Toggle.isOn = true;
    }

    private void HideAllSkinButtons()
    {
        foreach (SkinButton sb in this.skinButtons)
        {
            sb.gameObject.SetActive(false);
        }
    }

    public void ShowBallSkins()
    {
        this.HideAllSkinButtons();
        this.skinsShown = SkinsShown.Ball;
        for (int i = 0; i < SkinRepository.Instance.BallSkins.Length; i++)
        {
            this.skinButtons[i].gameObject.SetActive(true);
            this.skinButtons[i].Setup(this, SkinRepository.Instance.BallSkins[i], i);
            this.skinButtons[i].Toggle.isOn = false;
        }
        this.skinButtons[SkinRepository.Instance.CurrentBallSkinIndex].Toggle.isOn = true;

    }

    public void SkinSelected(SkinButton skin)
    {
        if (skin.AssignedSkin.Unlocked)
        {
            if (this.skinsShown == SkinsShown.Vortex)
            {
                SkinRepository.Instance.CurrentVortexSkinIndex = skin.SkinIndex;
            } else
            {
                SkinRepository.Instance.CurrentBallSkinIndex = skin.SkinIndex;
            }
           
            this.skinPreviewPanel.SetActive(false);
        } else {
            this.ShowSkinPreview(skin.AssignedSkin);
        }
    }

    private void ShowSkinPreview(Skin skin)
    {
        this.skinPreviewPanel.SetActive(true);
        this.selectedSkin = skin;
        this.skinPreviewImage.sprite = skin.PreviewImage;
        if (skin.Paid)
        {
            this.achievementText.text = string.Empty;
            this.achievementStatusText.text = Bank.Instance.Diamonds + "/" + skin.Price.ToString();
            this.skinBuyButton.gameObject.SetActive(true);
            this.skinPriceText.text = skin.Price.ToString();
            if (Bank.Instance.Diamonds < skin.Price)
            {
                this.skinBuyButton.interactable = false;
            } else
            {
                this.skinBuyButton.interactable = true;
            }

        } else
        {
            this.skinBuyButton.gameObject.SetActive(false);
            Achievement achi = skin.Achievement;
            this.achievementText.text = achi.Description;
            this.achievementStatusText.text = achi.CurrentStatus.ToString() + "/" + achi.Goal.ToString();
        }
    }

    public void BuySkin()
    {
        this.selectedSkin.BuySkin();
        this.UpdateSkinsUnlockedBar();
        if (this.skinsShown == SkinsShown.Vortex)
        {
            this.ShowVortexSkins();
        }
        else
        {
            this.ShowBallSkins();
        }
    }

    public void HideSkinPreview()
    {
        this.skinPreviewPanel.SetActive(false);
    }
}
