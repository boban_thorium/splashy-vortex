﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinPrefab : MonoBehaviour {
    [SerializeField]
    private ParticleSystem explosion;
    [SerializeField]
    private TrailRenderer trailRenderer;

    private MeshRenderer[] meshRenderers;

    private void Start()
    {
        this.meshRenderers = this.gameObject.GetComponentsInChildren<MeshRenderer>();
    }

    public void Hide()
    {
        foreach(MeshRenderer mr in this.meshRenderers)
        {
            mr.enabled = false;
        }
        this.trailRenderer.enabled = false;
    }

    public void Show()
    {
        foreach (MeshRenderer mr in this.meshRenderers)
        {
            mr.enabled = true;
        }
        this.trailRenderer.enabled = true;
    }

    public void Explode()
    {
        this.Hide();
        this.explosion.Emit(30);
    }
}
