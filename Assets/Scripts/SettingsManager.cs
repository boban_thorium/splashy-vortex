﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : Singleton<SettingsManager> {

    public bool AudioOn
    {
        get
        {
            int on = PlayerPrefs.GetInt("AudioOn", 1);
            if (on == 0)
            {
                return false;
            } else
            {
                return true;
            }
        }

        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("AudioOn", 1);
            }
            else
            {
                PlayerPrefs.SetInt("AudioOn", 0);
            }
        }
    }

    public bool VibrateOn
    {
        get
        {
            int on = PlayerPrefs.GetInt("VibrateOn", 1);
            if (on == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        set
        {
            if (value)
            {
                PlayerPrefs.SetInt("VibrateOn", 1);
            }
            else
            {
                PlayerPrefs.SetInt("VibrateOn", 0);
            }
        }
    }
}
