﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ScoreIndicator : MonoBehaviour {
    [SerializeField]
    private TextMeshPro displayText;
    [SerializeField]
    private UnityEvent onShowScore;
    [SerializeField]
    private string[] congratulationWords;

    public void ShowScore(int score, bool perfect)
    {
        string msg = "+" + score;
        if (perfect)
        {
            msg += " " + this.congratulationWords[Random.Range(0, this.congratulationWords.Length)];
        }
        this.displayText.SetText(msg);
        this.onShowScore.Invoke();
    }
}
