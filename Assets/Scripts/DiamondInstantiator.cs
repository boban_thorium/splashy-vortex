﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondInstantiator : MonoBehaviour {
    [SerializeField]
    [Range(0, 100)]
    private float possibility;
    [SerializeField]
    private GameObject diamondPrefab;
	void Start () {
        var spawn = Random.Range(0, 100);
        if (spawn <= this.possibility)
        {
            Instantiate(this.diamondPrefab, this.transform.position, this.transform.rotation, this.transform);
        }
	}
}
